#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <fftw3.h>

double *ELEMENT(double *a, int i, int j, int ny) { return a+ny*i+j; }
double *LELEMENT(double *a, int i, int j, int ny) { return a+ny*i+j; }


double amplitude(double x, double y) {
  return sqrt(x*x+y*y);
}

double diff_images(double *im1, double *im2, int nx, int ny) {
  double diff = 0;
  int i, j;
  for (i=0; i<nx; ++i)
    for (j=0; j<ny; ++j) {
      diff += ((*ELEMENT(im1,i,j,ny))-(*LELEMENT(im2,i,j,ny)))*((*ELEMENT(im1,i,j,ny))-(*LELEMENT(im2,i,j,ny)));
    }
  return sqrt(diff/nx/ny);
}

void normalize(double *a, int nx, int ny) {
  int i,j;
  double sum=0;

  for (i=0; i<nx; ++i)
    for (j=0; j<ny; ++j) 
      sum += *ELEMENT(a, i, j, ny);
  for (i=0; i<nx; ++i)
    for (j=0; j<ny; ++j) {
      *ELEMENT(a, i, j, ny)  *= (nx*ny)/sum;
    }
  sum = 0;
  for (i=0; i<nx; ++i)
    for (j=0; j<ny; ++j)
      sum += *ELEMENT(a, i, j, ny);
  printf("Normalize to %lf of %d %d\n",sum,nx,ny);
}

void complex_multiply(double x, double y, double u, double v, double *re, double *im) {
  *re = (x*u-y*v);  *im = x*v+y*u;
}

void write_map(double *a, int nx, int ny, const char *fname) {
  int i, j;
  FILE *f;
  f = fopen(fname,"w");
  fprintf(f,"%d\n",nx);
  for ( i = 0; i < nx; i++ ) {
    for ( j = 0; j < ny; j++ ) {
      fprintf(f,"%f\n",*LELEMENT(a,i,j,ny));
    }
  }
  fclose(f);
}

// http://people.sc.fsu.edu/~jburkardt/c_src/fftw3/fftw3_prb.c
int  main ( ) {
  FILE *f;
  int i, u, v;
  double *image, *in2, *image2;
  double *recon, *tmp;
  int j;
  int nx, ny;
  int nyh, x, y;
  fftw_plan plan_forward, plan_backward;
  fftw_complex *out, *out2;
  double sum_in, sum_out, max;
  double amp, phase;

/* A wave that has U wavelengths in the u direction and V wavelengths in the V direction
 will produce a Fourier component at U, V and H-U, W-V in the raw FFT grid numbering from (0, 0)
 at the corner.  
*/


  f = fopen("mona_small_small.dat","r"); fscanf(f,"%d\n",&nx); ny = nx;

  image = ( double * ) malloc ( sizeof ( double ) * nx * ny );
  recon = ( double * ) malloc ( sizeof ( double ) * nx * ny );


  sum_in = 0;
  // Load test file
  max = -1e39;
  for ( i = 0; i < nx; i++ ) {
    for ( j = 0; j < ny; j++ ) {
      fscanf(f,"%lf\n",ELEMENT(image,i,j,ny));
      if ( *ELEMENT(image,i,j,ny)  > max ) max = *ELEMENT(image,i,j,ny);
      *LELEMENT(recon,i,j,ny) = 0;
    }
  }
  fclose(f);

  sum_in = 0;
  for ( i = 0; i < nx; i++ ) {
    for ( j = 0; j < ny; j++ ) 
      sum_in += fabs(*ELEMENT(image, i, j,ny));
  }

  nyh = ( ny / 2 ) + 1;

  out = fftw_malloc ( sizeof ( fftw_complex ) * nx * nyh );
  plan_forward = fftw_plan_dft_r2c_2d ( nx, ny, image, out, FFTW_ESTIMATE );

  fftw_execute ( plan_forward );

  for ( i = 0; i < nx; i++ ) {
    printf("%d\n",i);
    for ( j = 0; j < nyh; j++ ) {
      if ( j != 0 && j!= nyh-1 ) { out[i*nyh+j][0] *= 2;  out[i*nyh+j][1] *= 2; }
      amp = amplitude(out[i*nyh+j][0], out[i*nyh+j][1]);
      phase = atan2(out[i*nyh+j][1], out[i*nyh+j][0]);
      u = i; v = j;
      for (x=0; x<nx; x++) 
        for (y=0; y<ny; y++)  {
if ( x == 0 && y == 1 ) printf("X %d %d %d %d %f %f %lf\n",x, y, u, v,amp,phase, amp*cos(2*M_PI*(u*(double)(x)/nx+v*(double)(y)/ny)+phase));
          *LELEMENT(recon,x,y,ny) += amp*cos(2*M_PI*(u*(double)(x)/nx+v*(double)(y)/ny)+phase);
        }      
    }
  }


  // Normalize
  sum_out = 0;
  for (i=0; i<nx; ++i)
    for (j=0; j<ny; ++j) sum_out += fabsl(*LELEMENT(recon, i, j,ny));
  for (i=0; i<nx; ++i)
    for (j=0; j<ny; ++j)
      *LELEMENT(recon, i, j,ny) *= sum_in/sum_out;
  for (i=0; i<nx; ++i)
    for (j=0; j<ny; ++j) {
      double  d = (*LELEMENT(recon, i, j,ny))-(*ELEMENT(image, i, j,ny));
      if ( fabs(d) > 1e14 ) printf("DIFF %f\n",d);
  }
  printf("Scale factor %f. Max %f. RMS diff %.14f.\n",sum_out/sum_in, max, diff_images(image, recon, nx,ny));
  write_map(recon, nx, ny, "recon.map");
  return 0;

}
