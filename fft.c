#include <fftw3.h>

// Keep the FFTW code away from cuda and nvcc

void do_fft(double *image, int nx, int ny, fftw_complex *output) {
  int nyh;
  fftw_plan plan_forward;
 
  plan_forward = fftw_plan_dft_r2c_2d(nx, ny, image, output, FFTW_ESTIMATE );

  fftw_execute(plan_forward);
  fftw_free(plan_forward);
}

