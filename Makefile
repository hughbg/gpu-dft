transform: transform.cu fft.c
	gcc -c fft.c
	nvcc transform.cu fft.o -o transform -lfftw3 -lm

transform_c:	transform.c fft.c
	gcc transform.c fft.c -o transform_c -lfftw3 -lm
