#include <stdio.h>
#include <math.h>
#include <malloc.h>

#define BLOCK 16
#define THREAD 32

typedef double fftw_complex[2];   // This needs to match true fftw_complex type in fftw3

extern "C" void do_fft(double *image, int nx, int ny, fftw_complex *out);

double *I_ELEMENT(double *a, int i, int j, int ny) { return a+ny*i+j; }

void write_map(double *a, int nx, int ny, const char *fname) {
  int i, j;
  FILE *f;
  f = fopen(fname,"w");
  fprintf(f,"%d\n",nx);
  for ( i = 0; i < nx; i++ ) {
    for ( j = 0; j < ny; j++ ) {
      fprintf(f,"%f\n",*I_ELEMENT(a,i,j,ny));
    }
  }
  fclose(f);
}

double diff_images(double *im1, double *im2, int nx, int ny) {
  double diff = 0;
  int i, j;
  for (i=0; i<nx; ++i)
    for (j=0; j<ny; ++j) {
      diff += ((*I_ELEMENT(im1,i,j,ny))-(*I_ELEMENT(im2,i,j,ny)))*((*I_ELEMENT(im1,i,j,ny))-(*I_ELEMENT(im2,i,j,ny)));
    }
  return sqrt(diff/nx/ny);
}

__device__ double *ELEMENT(double *a, int i, int j, int ny) { return a+ny*i+j; }

__device__ double amplitude(double x, double y) {
  return sqrt(x*x+y*y);
}

__global__ void reconstruct(fftw_complex *fourier_components, double *recons, int nx, int ny) {
  int i, j, nyh, x, y;
  double amp, phase, real, imag, sum=0;

  // Find the indexes of this thread into the image
  x = blockIdx.x*blockDim.x+threadIdx.x;
  y = blockIdx.y*blockDim.y+threadIdx.y;

  nyh = (ny/2)+1;

  for (i=0; i<nx; i++ ) {
    for (j=0; j<nyh; j++) {
      if ( j != 0 && j != nyh-1) { real = fourier_components[i*nyh+j][0]*2;  imag = fourier_components[i*nyh+j][1]*2; }
      else { real = fourier_components[i*nyh+j][0]; imag = fourier_components[i*nyh+j][1]; }
      amp = amplitude(real, imag);
      phase = atan2(imag, real);
      sum += amp*cos(2*M_PI*(i*(double)x/nx+j*(double)y/ny)+phase);      // x,y in here means the sum is different
    }
  }
  *ELEMENT(recons,x,y,ny) = sum;

}

int  main() {
  FILE *f;
  int i;
  double *image;
  int j;
  int nx, ny;
  int nyh;		
  fftw_complex *fourier_components;
  double *recons_image;
  dim3 grid_dim, block_dim;
  fftw_complex *gpu_fourier;
  double *gpu_image;

  f = fopen("mona.dat","r"); fscanf(f,"%d\n",&nx); ny = nx;

  image = (double*)malloc(sizeof(double)*nx*ny);

  // Load test file
  for ( i = 0; i < nx; i++ ) {
    for ( j = 0; j < ny; j++ ) {
      fscanf(f,"%lf\n",I_ELEMENT(image,i,j,ny));
    }
  }
  fclose(f);

  // Allocate memory for Fourier components and image CPU-side
  nyh = (ny/2)+1;
  fourier_components = (fftw_complex*)malloc(sizeof(fftw_complex)*nx*nyh );
  recons_image = (double*)malloc(sizeof(double)*nx*ny);

  // Make FFT
  do_fft(image, nx, ny, fourier_components);
 
  // Copy FFT to GPU
  if ( cudaMalloc(&gpu_fourier, sizeof(fftw_complex)*nx*nyh ) != cudaSuccess ) {
    fprintf(stderr,"Failed to allocate GPGPU memory for Fourier components.\n"); exit(1);
  } 
  if ( cudaMemcpy(gpu_fourier, fourier_components, sizeof(fftw_complex)*nx*nyh, cudaMemcpyHostToDevice) != cudaSuccess ) {
    fprintf(stderr,"Failed to copy Fourier components to device\n"); exit(1);
  }     

  // Make space on GPU for image
  if ( cudaMalloc(&gpu_image, sizeof(double)*nx*ny ) != cudaSuccess ) {
    fprintf(stderr,"Failed to allocate GPGPU memory for output image.\n"); exit(1);
  } 
  if ( cudaMemset(gpu_image, 0, sizeof(double)*nx*ny ) != cudaSuccess ) {
    fprintf(stderr,"Failed to zero GPGPU memory for image accumulation.\n"); exit(1);
  }


  // Run GPU

  grid_dim.x = 2*BLOCK; 						/* <- number of blocks in X direction. */
  grid_dim.y  = 2*BLOCK;					/* <- number of blocks in Y direction. */
  block_dim.x = THREAD/2; 						/* <- number of threads in X direction. */
  block_dim.y = THREAD/2; 			/* <- number of threads in Y direction.*/

  reconstruct<<<grid_dim, block_dim>>>(gpu_fourier, gpu_image, nx, ny);

  // Copy result back here
  if ( cudaMemcpy(recons_image, gpu_image, sizeof(double)*nx*ny, cudaMemcpyDeviceToHost) != cudaSuccess ) {
    fprintf(stderr,"Failed to copy image from device to host\n"); exit(1);
  }     

  // Normalize
  for ( i = 0; i < nx; i++ ) {
    for ( j = 0; j < ny; j++ ) 
      *I_ELEMENT(recons_image, i, j,ny) /= (nx*ny);
  }

  printf("RMS diff %.14f.\n",diff_images(image, recons_image, nx,ny));

  write_map(recons_image, nx, ny, "recon.map");

}

